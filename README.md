
## Package

This Project is used for test only. This Package API is using micro-framework of laravel (Lumen). Why using Lumen because Lumen is a micro framework that means the smaller, simpler, leaner and faster, Lumen is primarily used to build for microservices that have loosely coupled components that reduce complexity and enhances the improvements easily.

### Built With
* [Laravel-Lumen](https://lumen.laravel.com/)

## Getting Started

First clone the project into your local machines
### Installation

1. All data on this seeder and migration is refer from example json given
2. Clone the repo
```sh
https://gitlab.com/regitald/package.git
```
3. Whenever you clone a new Laravel/Lumen project you must now install all of the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.When we run composer, it checks the composer.json file which is submitted to the github repo and lists all of the composer (PHP) packages that your repo requires. Because these packages are constantly changing, the source code is generally not submitted to github, but instead we let composer handle these updates. So to install all this source code we run composer with the following command.
```sh
composer install
```
4. Enter your env 
```JS
//example --this env is what i used in my local
APP_NAME=Lumen
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=Asia/Jakarta

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=package
DB_USERNAME=root
DB_PASSWORD=

CACHE_DRIVER=file
QUEUE_CONNECTION=sync

```
5. create database and start run the terminal then type
 ```sh
php artisan migrate
```
6. run seeder database 
```sh
php artisan db:seed
```
7. Import the collection into your postman [Package Collection](https://www.getpostman.com/collections/88962d3321645d92dc82)
8. Make env for postman collection 
```sh
//this is example env for the postman
{
	"id": "#",
	"name": "PACKAGE",
	"values": [
		{
			"key": "URL",
			"value": "localhost/package/public/api/",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2020-08-11T18:52:39.116Z",
	"_postman_exported_using": "Postman/7.29.1"
}
```

## Usage

To use this project you need to import the collection first [Package Collection](https://www.getpostman.com/collections/88962d3321645d92dc82)

1. Crud is available on the collection
    1. GET PACKAGE DATA
    2. GET DETAIL PACKAGE DATA
    3. PUT
    4. PATCH
    5. POST / CREATE
    6. DELETE 

## Note

[API DOCS](https://documenter.getpostman.com/view/3484329/T1LLFoRD)

