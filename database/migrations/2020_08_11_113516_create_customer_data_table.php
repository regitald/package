<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_name');   
            $table->mediumText('customer_address'); 
            $table->string('customer_email')->nullable();  
            $table->string('customer_phone');   
            $table->text('customer_address_detail')->nullable();
            $table->string('customer_zip_code');   
            $table->integer('organization_id');   
            $table->string('zone_code'); 
            $table->string('location_id');
            $table->enum('type', [0, 1])->default(0)->comment('0 -> origin | 1 -> destination');
            $table->enum('deleted', [0, 1])->default(0)->comment('0 -> exist | 1 -> deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
