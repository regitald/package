<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id')->unique();
            $table->integer('transaction_order');
            $table->string('transaction_code')->unique();
            $table->double('transaction_amount')->default(0);
            $table->integer('transaction_discount')->default(0);
            $table->integer('transaction_payment_type');
            $table->string('transaction_payment_type_name');
            $table->string('transaction_state')->default("");
            $table->text('transaction_additional_field')->default("");
            $table->string('location_id')->default("");
            $table->integer('organization_id');
            $table->double('transaction_cash_amount')->default(0);
            $table->double('transaction_cash_change')->default(0);
            $table->text('custom_field')->default();
            $table->integer('agent_id')->default(0);
            $table->string('customer_origin_id');
            $table->string('customer_destination_id');
            $table->string('customer_name');
            $table->string('customer_code');
            $table->string('customer_attribute_id');
            $table->enum('deleted', [0, 1])->default(0)->comment('0 -> exist | 1 -> deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
