<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerattributeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_attribute', function (Blueprint $table) {
            $table->bigIncrements('customer_attribute_id');
            $table->string('Nama_Sales');   
            $table->string('TOP'); 
            $table->string('Jenis_Pelanggan');  
            $table->enum('deleted', [0, 1])->default(0)->comment('0 -> exist | 1 -> deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_attribute');
    }
}
