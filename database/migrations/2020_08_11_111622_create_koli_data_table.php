<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKoliDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koli_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('koli_id')->unique();
            $table->string('koli_code')->unique();
            $table->string('connote_id');
            $table->string('koli_description')->default("");
            $table->integer('koli_length')->default(0);
            $table->integer('koli_height')->default(0);
            $table->integer('koli_chargeable_weight')->default(0);
            $table->integer('koli_width')->default(0);
            $table->integer('koli_volume')->default(0);
            $table->integer('koli_weight')->default(0);
            $table->string('awb_url')->default("");
            $table->text('koli_surcharge');
            $table->integer('koli_formula_id')->nullable();
            $table->text('koli_custom_field');
            $table->enum('deleted', [0, 1])->default(0)->comment('0 -> exist | 1 -> deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koli_data');
    }
}
