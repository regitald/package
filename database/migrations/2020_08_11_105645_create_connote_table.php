<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connote', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('connote_id')->unique();
            $table->string('connote_code')->unique();
            $table->string('transaction_id');
            $table->integer('connote_number');
            $table->string('connote_service')->default("");
            $table->double('connote_service_price')->default(0);
            $table->double('connote_amount')->default(0);
            $table->string('connote_booking_code')->default("");
            $table->integer('connote_order');
            $table->string('connote_state');
            $table->integer('connote_state_id');
            $table->string('zone_code_from');
            $table->string('zone_code_to');
            $table->integer('surcharge_amount');
            $table->integer('actual_weight')->default(0);
            $table->integer('volume_weight')->default(0);
            $table->integer('chargeable_weight')->default(0);
            $table->integer('organization_id');
            $table->string('location_id');
            $table->string('connote_total_package');
            $table->string('connote_surcharge_amount');
            $table->string('connote_sla_day');
            $table->string('location_name');
            $table->string('location_type');
            $table->string('source_tariff_db');
            $table->string('id_source_tariff');
            $table->integer('pod');
            $table->text('history');
            $table->enum('deleted', [0, 1])->default(0)->comment('0 -> exist | 1 -> deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connote');
    }
}
