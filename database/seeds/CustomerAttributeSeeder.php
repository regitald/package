<?php

use Illuminate\Database\Seeder;

class CustomerAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CustomerAttributeModel::create([
            'Nama_Sales'=> 'Radit Fitrawikarsa',
            'TOP'=> '14 Hari',
            'Jenis_Pelanggan'=> 'B2B'
        ]);
    }
}
