<?php

use Illuminate\Database\Seeder;

class AgenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\AgenModel::create([
            'name'=> 'Hub Jakarta Selatan',
            'code'=> 'JKTS01',
            'type'=> 'Agent'
        ]);
    }
}
