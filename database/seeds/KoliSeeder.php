<?php

use Illuminate\Database\Seeder;

class KoliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\KoliModel::create([
            "koli_length"=> 0,
            "awb_url"=> "https=>\/\/tracking.mile.app\/label\/AWB00100209082020.2",
            "koli_chargeable_weight"=> 9,
            "koli_width"=> 0,
            "koli_surcharge"=> '',
            "koli_height"=> 0,
            "koli_description"=> "V WARP",
            "koli_formula_id"=> 0,
            "connote_id"=> "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
            "koli_volume"=> 0,
            "koli_weight"=> 9,
            "koli_id"=> "3600f10b-4144-4e58-a024-cc3178e7a709",
            "koli_code"=> "AWB00100209082020.2",
            'koli_custom_field'=> '{
                "awb_sicepat": null,
                "harga_barang": null
            }',
        ],[
            "koli_code"=> "AWB00100209082020.3",
            "koli_length"=> 0,
            "awb_url"=> "https=>\/\/tracking.mile.app\/label\/AWB00100209082020.3",
            "koli_chargeable_weight"=> 2,
            "koli_width"=> 0,
            "koli_surcharge"=> '',
            "koli_height"=> 0,
            "koli_description"=> "LID HOT CUP",
            "koli_formula_id"=> 0,
            "connote_id"=> "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
            "koli_volume"=> 0,
            "koli_weight"=> 2,
            "koli_id"=> "2937bdbf-315e-4c5e-b139-fd39a3dfd15f",
            'koli_custom_field'=> '{
                "awb_sicepat": null,
                "harga_barang": null
            }',
        ]);
    }
}
