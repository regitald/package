<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TransactionSeeder::class);
        $this->call(CustomerAttributeSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(AgenSeeder::class);
        $this->call(KoliSeeder::class);
        $this->call(ConnoteSeeder::class);
    }
}
