<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CustomerModel::create([
            'customer_name'=> 'PT. NARA OKA PRAKARSA',
            'customer_address'=> 'JL. KH. AHMAD DAHLAN NO. 100, SEMARANG TENGAH 12420',
            'customer_email'=> 'info@naraoka.co.id',
            'customer_phone'=> '024-1234567',
            'customer_address_detail'=> null,
            'customer_zip_code'=> '12420',
            'zone_code'=> 'CGKFT',
            'organization_id'=> 6,
            'location_id'=> '5cecb20b6c49615b174c3e74'
        ],[
            'customer_name'=> 'PT AMARIS HOTEL SIMPANG LIMA',
            'customer_address'=> 'JL. KH. AHMAD DAHLAN NO. 01, SEMARANG TENGAH',
            'customer_email'=> null,
            'customer_phone'=> '0248453499',
            'customer_address_detail'=> 'KOTA SEMARANG SEMARANG TENGAH KARANGKIDUL',
            'customer_zip_code'=> '50241',
            'zone_code'=> 'SMG',
            'organization_id'=> 6,
            'location_id'=> '5cecb20b6c49615b174c3e74'
        ]);
    }
}
