<?php

use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\TransactionModel::create([
            'transaction_id'=> 'd0090c40-539f-479a-8274-899b9970bddc',
            'customer_name'=> 'PT. AMARA PRIMATIGA',
            'customer_code'=> '1678593',
            'transaction_amount'=> '70700',
            'transaction_discount'=> '0',
            'transaction_additional_field'=> '',
            'transaction_payment_type'=> '29',
            'transaction_state'=>'PAID',
            'transaction_code'=> 'CGKFT20200715121',
            'transaction_order'=> 121,
            'location_id'=> '5cecb20b6c49615b174c3e74',
            'organization_id'=> 6,
            'transaction_payment_type_name'=> 'Invoice',
            'transaction_cash_amount'=> 0,
            'transaction_cash_change'=> 0,
            'custom_field'=> '{
                "catatan_tambahan": "JANGAN DI BANTING \/ DI TINDIH"
            }',
            'agent_id'=> 1,
            'customer_origin_id'=> 1,
            'customer_destination_id'=> 2,
            'customer_attribute_id'=> 1,
        ]);
    }
}
