<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function ResponseStatus($STATUS, $message, $data)
    {
        return response()->json([
            'STATUS' => $STATUS, 
            'MESSAGE' => $message,
            'DATA' => $data
        ], $STATUS);
    }
    protected function Validator(array $data, array $role, $first = true)
    {
        $validator = Validator::make($data, $role);
        if ($validator->fails()) {
            if (!$first) {
                $errors = $validator->getMessageBag()->first();

                foreach ($errors as $i=>$error) {
                    $errormsg[]['error'] = $error;
                }
                return $errormsg;
            }
            $errors = $validator->getMessageBag()->first();
            return $errors;
        }
    }
}
