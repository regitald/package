<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AgenModel;
use App\Models\ConnoteModel;
use App\Models\CustomerModel;
use App\Models\KoliModel;
use App\Models\TransactionModel;
use App\Models\CustomerAttributeModel;

class PackageController extends Controller
{

    private $agen;
    private $connote;
    private $customer;
    private $koli;
    private $transaction;

    public function __construct()
    {
        $this->agen         = AgenModel::select('*')->where('deleted','0');
        $this->connote      = ConnoteModel::select('*')->where('deleted','0');
        $this->customer     = CustomerModel::select('*')->where('deleted','0');
        $this->koli         = KoliModel::select('*')->where('deleted','0');
        $this->transaction  = TransactionModel::select('*')->where('deleted','0');
        $this->attribute  = CustomerAttributeModel::select('*')->where('deleted','0');
    }
    public function index(){
        $data = $this->transaction->get();
        if (!$data->isEmpty()) {
            $collect1 = collect($this->connote->get());
            $collect2 =  collect($this->attribute->get());
            $collect3 =  collect($this->customer->get());
            $collect4 =  collect($this->koli->get());
            $collect5 =  collect($this->agen->get());
            $data = $data->map(function($key) use($collect1,$collect2,$collect3,$collect4,$collect5){
                $key['customer_attribute']   = $collect2->where('customer_attribute_id', $key['customer_attribute_id'])->first();
                $key['connote']              = $collect1->where('transaction_id', $key['transaction_id'])->first();
                $key['connote_id']           = $collect1->where('transaction_id', $key['transaction_id'])->pluck('connote_id')->first();
                $key['origin_data']          = $collect3->where('id', $key['customer_origin_id'])->first();
                $key['destination_data']     = $collect3->where('id', $key['customer_destination_id'])->first();
                $key['koli_data']            = $collect4->where('connote_id', $key['connote_id']);
                $key['currentLocation']      = $collect5->where('id', $key['agen_id'])->first();
                return $key;
            });
            return $this->ResponseStatus(200, 'SUCCESS', $data);
        }else{
            return $this->ResponseStatus(404, 'FAILED! Data Not Found!', array());
        }

    }
    public function detail($id){
        if (empty($id)) {
            return $this->ResponseStatus(400, 'Failed Validator Error! You need to pass id', new \stdClass());
        }
        $data = $this->transaction->where('transaction_id',$id)->get();
        if (!$data->isEmpty()) {
            $collect1 = collect($this->connote->get());
            $collect2 =  collect($this->attribute->get());
            $collect3 =  collect($this->customer->get());
            $collect4 =  collect($this->koli->get());
            $collect5 =  collect($this->agen->get());
            $data = $data->map(function($key) use($collect1,$collect2,$collect3,$collect4,$collect5){
                $key['customer_attribute']   = $collect2->where('customer_attribute_id', $key['customer_attribute_id'])->first();
                $key['connote']              = $collect1->where('transaction_id', $key['transaction_id'])->first();
                $key['connote_id']           = $collect1->where('transaction_id', $key['transaction_id'])->pluck('connote_id')->first();
                $key['origin_data']          = $collect3->where('id', $key['customer_origin_id'])->first();
                $key['destination_data']     = $collect3->where('id', $key['customer_destination_id'])->first();
                $key['koli_data']            = $collect4->where('connote_id', $key['connote_id']);
                $key['currentLocation']      = $collect5->where('id', $key['agen_id'])->first();
                return $key;
            });
            return $this->ResponseStatus(200, 'SUCCESS', $data[0]);
        }else{
            return $this->ResponseStatus(404, 'FAILED! Data Not Found!', array());
        }
    }
    public function delete($id){
        if (empty($id)) {
            return $this->ResponseStatus(400, 'Failed Validator Error! You need to pass id', new \stdClass());
        }
        $post['deleted'] ='1';
        $delete = TransactionModel::where('transaction_id','=',$id)->update($post);
        if (!empty($delete)) {
            return $this->ResponseStatus(200, 'Success! Delete Package!', new \stdClass());
        }else{
            return $this->ResponseStatus(400, 'Failed! Delete Package!', new \stdClass());
        }
    }
    public function add(Request $request){
        $PostTrx    = $request->only('transaction_id',
        'transaction_order',
        'transaction_code',
        'transaction_amount',
        'transaction_discount',
        'transaction_payment_type',
        'transaction_payment_type_name',
        'transaction_state',
        'transaction_additional_field',
        'location_id',
        'organization_id',
        'transaction_cash_amount',
        'transaction_cash_change',
        'customer_name',
        'customer_code',
        'custom_field'
        );
        $role = [
        'transaction_id' => 'Required',
        'transaction_order' => 'Required',
        'transaction_amount' => 'Required|numeric',
        'transaction_discount' => 'Required|numeric',
        'transaction_payment_type' => 'Required|numeric',
        'transaction_payment_type_name' => 'Required',
        'customer_name' => 'Required',
        'customer_code' => 'Required',
        ];
        $ErrorMsg = $this->Validator($PostTrx, $role);
        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus('400', 'Failed! ' . $ErrorMsg, new \stdClass());
        }
        // untuk customer_attribute
        $customer_attribute = CustomerAttributeModel::create($request['customer_attribute']);
        $connote           = ConnoteModel::create($request['connote']);
        //untuk customer destination & origin
        $CustomerOrigin = $this->customer($request['origin_data']);
        $CustomerDestination = $this->customer($request['destination_data'],1);

        //untuk koli_data
        if(!empty($request['koli_data'])){
            for ($i=0; $i < count($request['koli_data']); $i++) { 
                $KoliData = $this->kolidata($request['koli_data'][$i]);
                
            }
        }
        //current location
        $PostLocationAgent    = $request->only('name','code','type');
        $LocationAgent        = AgenModel::create($request['currentLocation']);
        //current trx

        $PostTrx['agent_id'] = $LocationAgent['id'];
        $PostTrx['customer_origin_id'] = $CustomerOrigin['id'];
        $PostTrx['customer_destination_id'] = $CustomerDestination['id'];
        $PostTrx['customer_attribute_id'] = $customer_attribute['id'];
        $PostTrx['custom_field'] = json_encode($request['custom_field']);

        $saved = TransactionModel::create($PostTrx);
        if (!$saved) {
            return $this->ResponseStatus('500', 'Failed! Data not saved', array());
        }else{
            return $this->ResponseStatus('200', 'Success! Add Package Data', array());
        }
    }
    public function update(Request $request,$id=""){
        
        $detail = $this->transaction->where('transaction_id','=',$id)->first();
        if (empty($detail)) {
            return $this->ResponseStatus('404', 'Failed! Data in this transaction not found!', array());
        }
        $PostTrx    = $request->only(
                                    'transaction_order',
                                    'transaction_code',
                                    'transaction_amount',
                                    'transaction_discount',
                                    'transaction_payment_type',
                                    'transaction_payment_type_name',
                                    'transaction_state',
                                    'transaction_additional_field',
                                    'location_id',
                                    'organization_id',
                                    'transaction_cash_amount',
                                    'transaction_cash_change',
                                    'customer_name',
                                    'customer_code',
                                    'custom_field'
                        );
        $PostTrx['custom_field'] = json_encode($request['custom_field']);
        $update = $this->transaction->where('transaction_id','=',$request['transaction_id'])->update($PostTrx);
        if (!$update) {
            return $this->ResponseStatus('500', 'Failed! Data not saved', array());
        }else{
            return $this->ResponseStatus('200', 'Success! Updated Package Data', array());
        }
    }
    public function kolidata($post){
        $PostKoli['koli_length']          = $post['koli_length'];
        $PostKoli['awb_url']              = $post['awb_url'];
        $PostKoli['koli_chargeable_weight']   = $post['koli_chargeable_weight'];
        $PostKoli['koli_width']           = $post['koli_width'];
        $PostKoli['koli_surcharge']       = $post['koli_surcharge'];
        $PostKoli['koli_height']          = $post['koli_height'];
        $PostKoli['koli_description']     = $post['koli_description'];
        $PostKoli['koli_formula_id']      = $post['koli_formula_id'];
        $PostKoli['connote_id']           = $post['connote_id'];
        $PostKoli['koli_volume']          = $post['koli_volume'];
        $PostKoli['koli_weight']          = $post['koli_weight'];
        $PostKoli['koli_id']              = $post['koli_id'];
        $PostKoli['koli_custom_field']    = json_encode($post['koli_custom_field']);
        $PostKoli['koli_code']            = $post['koli_code'];
        return KoliModel::create($PostKoli);
    }
    public function customer($post,$type=""){
        $PostOrigin['customer_name']    = $post['customer_name'];
        $PostOrigin['customer_address'] = $post['customer_address'];
        $PostOrigin['customer_email']   = $post['customer_email'];
        $PostOrigin['customer_phone']   = $post['customer_phone'];
        $PostOrigin['customer_zip_code']= $post['customer_zip_code'];
        $PostOrigin['zone_code']        = $post['zone_code'];
        $PostOrigin['organization_id']  = $post['organization_id'];
        $PostOrigin['location_id']      = $post['location_id'];
        $PostOrigin['customer_address_detail']   = $post['customer_address_detail'];
        if(!empty($type)){
            $PostOrigin['type'] = $type;
        }
        return CustomerModel::create($PostOrigin);
    }

}