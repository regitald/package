<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
	protected $table = 'customer';
	public $primarykey = 'id';
	public $timestamps = true;

	protected $fillable = [
		'id',
		'customer_name',   
		'customer_address', 
		'customer_email',  
		'customer_phone',   
		'customer_address_detail', 
		'customer_zip_code',   
		'zone_code',  
		'organization_id',   
		'location_id',
		'type',
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted'
	];

}