<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
	protected $table = 'transaction';
	public $primarykey = 'id';
	public $timestamps = true;

	protected $fillable = [
		'transaction_id',
		'transaction_order',
		'transaction_code',
		'transaction_amount',
		'transaction_discount',
		'transaction_payment_type',
		'transaction_payment_type_name',
		'transaction_state',
		'transaction_additional_field',
		'location_id',
		'organization_id',
		'transaction_cash_amount',
		'transaction_cash_change',
		'custom_field',
		'agent_id',
		'customer_origin_id',            
		'customer_destination_id',       
		'customer_name',       
		'customer_code', 
		'customer_attribute_id'
	];
	protected $casts = [
		'transaction_id' 			     => 'string',
		'customer_id' 			         => 'integer',
		'transaction_code' 			     => 'string',
		'transaction_amount' 			 => 'double',
		'transaction_discount' 			 => 'integer',
		'transaction_payment_type' 		 => 'integer',
		'transaction_payment_type_name'  => 'string',
		'transaction_state' 		 	 => 'string',
		'transaction_additional_field' 	 => 'string',
		'location_id' 					 => 'string',            
		'organization_id' 				 => 'integer',
		'transaction_cash_amount'        => 'double',
		'transaction_cash_change'		 => 'double',
		'custom_field'					 => 'string',
		'agent_id'						 => 'integer',
		'customer_origin_id'			 => 'integer',
		'customer_destination_id'		 => 'integer',
	];
		
	protected $hidden = [
		'id',
		'created_at',
		'updated_at',
		'deleted'
	];

}