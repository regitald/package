<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnoteModel extends Model
{
	protected $table = 'connote';
	public $primarykey = 'id';
	public $timestamps = true;

	protected $fillable = [
		'connote_id',
		'connote_code',
		'transaction_id',
		'connote_number',
		'connote_service',
		'connote_service_price',
		'connote_amount',
		'connote_booking_code',
		'connote_order',
		'connote_state',
		'connote_state_id',
		'zone_code_from',
		'zone_code_to',
		'surcharge_amount',
		'actual_weight',
		'volume_weight',            
		'chargeable_weight',
		'organization_id',
		'location_id',            
		'connote_total_package',
		'connote_surcharge_amount',
		'connote_sla_day',            
		'location_name',         
		'location_type',       
		'source_tariff_db',    
		'id_source_tariff',  
		'pod', 
		'history',
	];
	protected $casts = [
		'transaction_id' 			     => 'string',
		'connote_id' 			         => 'string',
		'connote_code' 			     	=> 'string',
		'connote_number' 			 	=> 'integer',
		'connote_service' 			 	=> 'string',
		'connote_service_price' 		=> 'double',
		'connote_amount'  				=> 'double',
		'connote_booking_code' 		 	=> 'string',
		'connote_order' 	 			=> 'integer',
		'connote_state' 				=> 'string',            
		'connote_state_id' 				=> 'integer',
		'zone_code_from'        		=> 'string',
		'zone_code_to'		 			=> 'string',
		'custom_field'					=> 'string',
		'surcharge_amount'				=> 'integer',
		'actual_weight'			 		=> 'integer',
		'chargeable_weight'		 		=> 'integer',
		'organization_id'			 	=> 'integer',
		'location_id'		 			=> 'string',        
		'connote_total_package'			=> 'string',  
		'connote_surcharge_amount'		=> 'string',  
		'connote_sla_day'				=> 'string',  
		'location_name'					=> 'string',          
		'location_type'					=> 'string',  
		'source_tariff_db'				=> 'string',     
		'id_source_tariff'				=> 'string',  
		'pod'							=> 'integer',    
		'history'						=> 'string', 
	];
		
	protected $hidden = [
		'id',
		'created_at',
		'updated_at',
		'deleted'
	];

}