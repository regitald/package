<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KoliModel extends Model
{
	protected $table = 'koli_data';
	public $primarykey = 'id';
	public $timestamps = true;

	protected $fillable = [
		'id',
		'koli_id',
		'koli_code',
		'connote_id',
		'koli_length',
		'koli_height',
		'koli_chargeable_weight',
		'koli_width',
		'koli_volume',
		'koli_weight',
		'awb_url',
		'koli_surcharge',
		'koli_formula_id',
		'koli_description',
		'koli_custom_field'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted'
	];

}