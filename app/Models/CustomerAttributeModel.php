<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CustomerAttributeModel extends Model
{
	protected $table = 'customer_attribute';
	public $primarykey = 'customer_attribute_id';
	public $timestamps = true;

	protected $fillable = [
		'customer_attribute_id',
		'Nama_Sales',
		'Jenis_Pelanggan',
		'TOP'
	];
	protected $casts = [
		'Nama_Sales' 			 => 'string',
		'Jenis_Pelanggan' 		=> 'string',
		'TOP' 			         => 'string'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted'
	];

}