<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgenModel extends Model
{
	protected $table = 'agent';
	public $primarykey = 'agent_id';
	public $timestamps = true;

	protected $fillable = [
		'agent_id',
		'code',
		'name',
		'type'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted'
	];

}