<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('package', 'PackageController@index');
    $router->get('package/{id}', 'PackageController@detail');
    $router->post('package/add', 'PackageController@add');
    $router->put('package/update/{id}', 'PackageController@update');
    $router->patch('package/update/{id}', 'PackageController@update');
    $router->delete('package/{id}', 'PackageController@delete');
});